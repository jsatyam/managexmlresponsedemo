//
//  StationList.swift
//  ManageXmlResponseDemo
//
//  Created by Jyotiraditya Satyam on 07/01/22.
//

import Foundation

class StationList: Decodable {
    var LogoPl: String!
    var StationId: String!
    var StationName: String!
}
