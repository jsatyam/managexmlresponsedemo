//
//  Extension+UIViewController.swift
//  ManageXmlResponseDemo
//
//  Created by Jyotiraditya Satyam on 06/01/22.
//

import UIKit

extension UIViewController {
    func showAlert(title: String = "Alert", message: String) {
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
    }
}
