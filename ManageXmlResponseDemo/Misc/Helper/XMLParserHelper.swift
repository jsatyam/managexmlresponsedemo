//
//  XMLParserHelper.swift
//  ManageXmlResponseDemo
//
//  Created by Jyotiraditya Satyam on 07/01/22.
//

import Foundation

final class XMLParserHelper: NSObject {
    
    private var recordKey = ""
    private var parser = XMLParser()
    private var currentValue: String?
    private var dictionaryKeys: Set<String> = []
    private var results: [[String: Any]] = []
    private var currentDictionary: [String: Any]?
    
    private func createXmlParserObject(xmldata: Data) {
        parser = XMLParser.init(data: xmldata)
        parser.delegate = self
        parser.parse()
    }
    
    // Pass Xml Data and Fetch Dictionary From it.
    func getDictionaryFromXmlData(xmldata: Data, recordKey: String, dictionaryKeys: Set<String>) -> [[String: Any]] {
        self.recordKey = recordKey
        self.dictionaryKeys = dictionaryKeys
        createXmlParserObject(xmldata: xmldata)
        return results
    }
    
    // We need to initially convert String to Data
    func getDataFromXmlData(xmldata: Data, recordKey: String, dictionaryKeys: Set<String>) -> Data? {
        let resultsDictionary = getDictionaryFromXmlData(xmldata: xmldata, recordKey: recordKey, dictionaryKeys: dictionaryKeys)
        do {
            let data = try JSONSerialization.data(withJSONObject: resultsDictionary, options: .prettyPrinted)
            return data
        } catch{
            print(error)
        }
        return nil
    }
    
}

// MARK: - XMLParser delegate methods
extension XMLParserHelper: XMLParserDelegate {
    func parserDidStartDocument(_ parser: XMLParser) {
        results = []
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == recordKey {
            currentDictionary = [:]
        } else if dictionaryKeys.contains(elementName) {
            currentValue = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if currentValue != nil {
            currentValue! += string
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == recordKey {
            results.append(currentDictionary!)
            currentDictionary = nil
        } else if dictionaryKeys.contains(elementName) {
            currentDictionary![elementName] = currentValue
            currentValue = nil
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("parseErrorOccurred: \(parseError)")
    }
}
