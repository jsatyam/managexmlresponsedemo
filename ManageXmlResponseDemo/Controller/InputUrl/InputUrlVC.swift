//
//  InputUrlVC.swift
//  ManageXmlResponseDemo
//
//  Created by Jyotiraditya Satyam on 06/01/22.
//

import UIKit

class InputUrlVC: UIViewController {
    
    // MARK: - Instance properties
    private let inputTextField = UITextField()
    private let submitButton = UIButton()
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private helper methods
    private func initialSetup() {
        view.backgroundColor = .white
        addSubViewInView()
        setUIConstraint()
    }
    
    private func addSubViewInView() {
        view.addSubview(inputTextField)
        inputTextField.placeholder = "Enter URL"
        inputTextField.borderStyle = .roundedRect
        
        view.addSubview(submitButton)
        submitButton.layer.cornerRadius = 6
        submitButton.backgroundColor = .black
        submitButton.setTitle("Submit", for: .normal)
        submitButton.setTitleColor(.white, for: .normal)
        submitButton.addTarget(self, action: #selector(nextController), for: .touchUpInside)
    }
    
    private func setUIConstraint() {
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        inputTextField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        inputTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputTextField.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 100).isActive = true
        inputTextField.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor, constant: 10).isActive = true
        
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.widthAnchor.constraint(equalToConstant: 160).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        submitButton.centerXAnchor.constraint(equalTo: inputTextField.centerXAnchor).isActive = true
        submitButton.topAnchor.constraint(equalTo: inputTextField.bottomAnchor, constant: 60).isActive = true
    }
    
    @objc private func nextController() {
        guard let inputStringUrl = inputTextField.text else {
            showAlert(message: "Please enter url")
            return
        }
        guard let inputUrl = URL(string: inputStringUrl) else {
            showAlert(message: "Please enter valid url")
            return
        }
        
        let stationlistVC = StationListVC()
        stationlistVC.inputUrl = inputUrl
        navigationController?.pushViewController(stationlistVC, animated: true)
    }
}

