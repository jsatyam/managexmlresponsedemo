//
//  StationListVM.swift
//  ManageXmlResponseDemo
//
//  Created by Jyotiraditya Satyam on 06/01/22.
//

import Foundation

class StationListVM: NSObject {
    
    // MARK: - Instance properties
    private let inputUrl: URL!
    var stationList = [StationList]()
    
    // MARK: - Init methods
    init(inputUrl: URL) {
        self.inputUrl = inputUrl
    }
}

// MARK: - API Calling
extension StationListVM {
    func setStationList(completion: @escaping (Bool, String) -> Void) {
        HudView.show()
        let dataTask = URLSession.shared.dataTask(with: inputUrl) { [weak self] data, response, error in
            HudView.hide()
            guard let self = self else { return }
            
            if let error = error {
                completion(false, error.localizedDescription)
                return
            }
            
            if let urlResponse = response as? HTTPURLResponse {
                let statusCode = urlResponse.statusCode
                if statusCode != 200 {
                    completion(false, "Status code \(statusCode)")
                    return
                }
            }
            
            if let data = data {
                let xmlParseHelper = XMLParserHelper()
                if let responseData = xmlParseHelper
                    .getDataFromXmlData(xmldata: data, recordKey: "Item", dictionaryKeys: ["StationId", "StationName", "LogoPl"]),
                   var stationList = try? JSONDecoder().decode([StationList].self, from: responseData) {
                    stationList.remove(at: 0)
                    self.stationList = stationList
                    DispatchQueue.main.async {
                        completion(true, "")
                    }
                }
            }
        }
        dataTask.resume()
    }
}
