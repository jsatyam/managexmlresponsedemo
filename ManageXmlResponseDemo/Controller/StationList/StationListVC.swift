//
//  StationListVC.swift
//  ManageXmlResponseDemo
//
//  Created by Jyotiraditya Satyam on 06/01/22.
//

import UIKit

class StationListVC: UITableViewController {
    
    // MARK: - Instance properties
    var inputUrl: URL!
    private var stationVM: StationListVM!
    
    // MARK: - View controller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private helper methods
    private func initialSetup() {
        stationVM = StationListVM(inputUrl: inputUrl)
        stationVM.setStationList { [weak self] success, message in
            guard let self = self else { return }
            if !success {
                self.showAlert(message: message)
                return
            }
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stationVM.stationList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let stationData = stationVM.stationList[indexPath.row]
        cell.textLabel?.text = stationData.StationName
        cell.detailTextLabel?.text = stationData.StationId
        if let logoUrlString = stationData.LogoPl {
            cell.imageView?.imageFromServerURL(urlString: logoUrlString, PlaceHolderImage: #imageLiteral(resourceName: "picture"))
        }
        return cell
    }
    
}
